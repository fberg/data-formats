#!/usr/bin/env python
"""create ROOT, HDF5 and ASCII files to compare

Generate a large set of random ints and floats and store them in various file
formats. This should allow a meaningfor comparison of data size in these formats.
"""
import h5py
import numpy as np
import ROOT
import sys


def main():
    data = np.random.random_sample((1000000, 3))

    # write ASCII encoded table
    np.savetxt(
        "sample.csv", data, delimiter=",", encoding="ascii", header="some, young, guy"
    )

    # write HDF5 table
    h5file = h5py.File("sample.h5", "w")
    h5file.create_dataset("some", data=data[:, 0])
    h5file.create_dataset("young", data=data[:, 1])
    h5file.create_dataset("guy", data=data[:, 2])
    h5file.close()

    # write a ROOT TTree
    print(repr(data[:, 0]))
    rdf = ROOT.RDF.MakeNumpyDataFrame(
        {"some": data[:, 0], "young": data[:, 1], "guy": data[:, 2]}
    )
    opts = ROOT.RDF.RSnapshotOptions()
    opts.fCompress = 505  # ZSTD with compressions level 5
    rdf.Snapshot("tree", "sample.root", "", options=opts)


if __name__ == "__main__":
    main()
